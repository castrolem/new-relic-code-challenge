# New Relic Apdex Board - Code challenge

We want to build a new page view that shows the list of applications running on every host.
* An application has a name, a list of contributors, a release version number, and a list of
hosts that the app has been deployed at.
* Each application has an Apdex metric assigned. The Apdex score is a positive integer
between 0 (Frustrated) and 100 (Satisfied). Apdex is an industry standard to measure users' satisfaction based on the response time of web applications and services.

## Important
* Each application list is always ordered by Apdex. The first app is the one with the highest Apdex. From top to bottom, most satisfied to most frustrated app.
* For this specific solution, you not need to worry about changes on the Apdex metric of an application.
* Please provide source code, not the compiled code.

Data
* [Example data JSON file](https://drive.google.com/file/d/0B88HSu672Sp9RVRQa3lnLTY4Mmc/view?usp=sharing)
* [UI mockups](https://drive.google.com/open?id=0B88HSu672Sp9Mk1UMDVpUnJwdnc)


## Setup Process
* Clone locally and cd into folder.
* Make sure you have `node ~8.2.1` and `npm ~5.3.0` installed.
* run `yarn install`, *If you don't have yarn installed, follow these [instructions](https://yarnpkg.com/lang/en/docs/install/)*.
* `npm run start-dev-server` and then go to `http://localhost:8080/` to see the site.
* `npm test` To run all the specs.
