const generateAppList = (apps) => {
  return apps.map(app => `
    <li>
        <p>
            <span class="apdex grayed">
                ${app.apdex}
            </span>
            <a href="#" data-version="${app.version}" data-contributors="${app.contributors.join(', ')}" class="link app--link">
                ${app.name}
            </a>
        </p>
    </li>
  `)
};

const appComponent = (data) => {
  return (
    `
      <li class="app box-layout">
            <header>
                <h5>${data.host}</h5>
            </header>
            <div class="content">
                <ol class="list-reset">
                    ${generateAppList(data.apps).join('')}
                </ol>
            </div>
        </li>
    `
  )
};

export default appComponent;
