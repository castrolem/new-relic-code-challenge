import appComponent from './app-component';
import {getApps, getTopAppsByHost} from '../actions/app-actions';
import {flatten} from '../utilities/array-utilities';

export const shapeAppsObject = (apps) => {
  let hosts = flatten(apps.map((app) => app.host));
  let uniqHosts = [ ...new Set(hosts) ];

  return uniqHosts.map(host => ({
    host,
    apps: getTopAppsByHost(host, 5)
  }));
};

const Apps = () => {
  window.apps = getApps();
  let appsToDisplay = flatten(shapeAppsObject(window.apps));
  return appsToDisplay.map((app) => appComponent(app)).join('');
};

export default Apps;
