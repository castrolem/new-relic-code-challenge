export const getData = (file) => {
  let rawFile = new XMLHttpRequest();
  let data = null;
  rawFile.open("GET", file, false);
  rawFile.onreadystatechange = function () {
    if (rawFile.readyState === 4) {
      if (rawFile.status === 200 || rawFile.status == 0) {
        data = JSON.parse(rawFile.responseText);
      }
    }
  };
  rawFile.send(null);
  return data;
};
