import {getData} from '../api/app-api.js';
import {getWindow} from '../utilities/window-utilities';
import {flatten} from '../utilities/array-utilities';

export const getApps = () => {
  return getData("/public/host-app-data.json");
};

export const getTopAppsByHost = (hostName, count = 25) => {
  let apps = getWindow().apps;
  let appsByHostName = apps.filter((app) => app.host.indexOf(hostName) > -1);
  appsByHostName.sort((a, b) => b.apdex > a.apdex ? 1 : -1);
  return appsByHostName.slice(0, count);
};

export const addAppToHosts = (hostNames, data) => {
  if(getWindow().apps) {
    let appFound = getWindow().apps.find((app) => app.name === data.name);
    if(appFound) {
      hostNames.forEach((name) => {
        let hostNamesFound = appFound.host.includes(name);
        if(!hostNamesFound) {
          appFound.host.push(name);
        }
      });
    } else {
      let params = {
        ...data,
        host: hostNames
      };
      getWindow().apps.push(params);
    }

    return getWindow().apps;
  } else {
    console.error('Couldn\'t find stored data in Window Object');
  }
};

export const removeAppFromHost = (hostName, app) => {
  let hostNameIdx = app.host.indexOf(hostName);
  if (hostNameIdx > -1) {
    app.host.splice(hostNameIdx, 1);
    return true
  }

  return false;
};

export const removeAppFromHosts = (hostNames, appName, minCount = 25) => {
  // minCount makes it easier to write specs without having to add lots of fake data
  if(getWindow().apps) {
    let filteredApps = [];
    hostNames.forEach((hostName) => {
      let hostApps = getTopAppsByHost(hostName);
      if (hostApps.length > minCount ) {
        let appFound = hostApps.find((app) => app.name === appName);

        if(appFound && removeAppFromHost(hostName, appFound)) {
          hostApps.splice(hostApps.indexOf(appFound), 1);
          filteredApps.push(hostApps);
        }
      }
    });
    return flatten(filteredApps);
  } else {
    console.error('Couldn\'t find stored data in Window Object');
  }
};

export const AppListeners = {
  _getAppDetails: () => {
    document.getElementById('apps').addEventListener('click', function (e) {
      if (e.target.classList.contains('app--link')) {
        e.preventDefault();
        alert(`App Version: ${e.target.dataset.version}\nContributors: ${e.target.dataset.contributors}`);
      }
    });
  },

  _toggleView: () => {
    let toggler = document.getElementById('toggle-view');
    let togglerTxt = document.querySelector('#toggle-view__label > span');
    let list = document.getElementById('apps');
    let userInfo = document.getElementById('user--info');

    toggler.addEventListener("click", function (e) {
      if (list.classList.contains('grid--layout')) {
        list.classList.remove('grid--layout');
        togglerTxt.textContent = 'Show as an awesome grid';
        userInfo.classList.add('truncate');
      } else {
        list.classList.add('grid--layout');
        togglerTxt.textContent = 'Show as List';
        userInfo.classList.remove('truncate');
      }
    }, false);
  },

  init: () => {
    document.addEventListener("DOMContentLoaded", () => {
      AppListeners._toggleView();
      AppListeners._getAppDetails();
    });
  }
};

