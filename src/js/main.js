import {AppListeners} from './actions/app-actions';
import Apps from './components/apps-component';
import '../styles/main.scss';

const AppInit = {
  init: () => {
    document.addEventListener("DOMContentLoaded", () => {
      const container = document.getElementById('apps');
      container.innerHTML = Apps();
    });
    AppListeners.init();
  }
};

AppInit.init();
