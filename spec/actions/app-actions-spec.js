import * as ApiFunctions from '../../src/js/api/app-api';
import * as WindowUtilities from '../../src/js/utilities/window-utilities';
import {getTopAppsByHost, addAppToHosts, removeAppFromHosts, removeAppFromHost} from '../../src/js/actions/app-actions.js';

describe('Actions', () => {
  describe('#getTopAppsByHost', () => {
    const applications = [
      {
        apdex: 33,
        contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
        host: ["92116865-5462.conor.com", "128406fc-0d3f.tiana.biz", "e7bf58af-f0be.dallas.biz", "7e6272f7-098e.dakota.biz"],
        name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
        version: 7
      },
      {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      },
      {
        apdex: 40,
        contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Small Rubber Sausages - Kutch LLC, and Sons",
        version: 5
      },
      {
        apdex: 50,
        contributors: ["Mr. Jarvis Hammes", "Rey Reichert"],
        host: ["e7bf58af-f0be.dallas.biz"],
        name: "Practical Fresh Chips - Weber - Lemke, Inc",
        version: 2
      },
      {
        apdex: 77,
        contributors: ["Mrs. Shaina Gusikowski", "Glenda Friesen"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Handcrafted Cotton Bacon - Gislason - Beer, and Sons",
        version: 3
      }
    ];

    beforeEach(() => {
      spyOn(ApiFunctions, 'getData').and.returnValue(applications);
      spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});
    });

    describe('with a valid hostname', () => {
      it('retrieves a JSON object from the parsed file', () => {
        const expectedOutput = [
          {
            apdex: 99,
            contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
            host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
            name: "Sleek Granite Chips - Labadie Inc, LLC",
            version: 4
          },
          {
            apdex: 77,
            contributors: ["Mrs. Shaina Gusikowski", "Glenda Friesen"],
            host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
            name: "Handcrafted Cotton Bacon - Gislason - Beer, and Sons",
            version: 3
          },
          {
            apdex: 40,
            contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
            host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
            name: "Small Rubber Sausages - Kutch LLC, and Sons",
            version: 5
          }
        ];
        const actualOutput = getTopAppsByHost('7e6272f7-098e.dakota.biz', 3);
        expect(actualOutput).toEqual(expectedOutput)
      });
    });

    it('returns an empty array', () => {
      const expectedOutput = [];
      const actualOutput = getTopAppsByHost('custom name that wont be found');
      expect(actualOutput).toEqual(expectedOutput)
    });
  });

  describe('#addAppToHosts', () => {
    const applications = [
      {
        apdex: 33,
        contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
        host: ["92116865-5462.conor.com", "128406fc-0d3f.tiana.biz", "e7bf58af-f0be.dallas.biz", "7e6272f7-098e.dakota.biz"],
        name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
        version: 7
      },
      {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      },
      {
        apdex: 40,
        contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Small Rubber Sausages - Kutch LLC, and Sons",
        version: 5
      },
      {
        apdex: 50,
        contributors: ["Mr. Jarvis Hammes", "Rey Reichert"],
        host: ["e7bf58af-f0be.dallas.biz"],
        name: "Practical Fresh Chips - Weber - Lemke, Inc",
        version: 2
      },
      {
        apdex: 77,
        contributors: ["Mrs. Shaina Gusikowski", "Glenda Friesen"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Handcrafted Cotton Bacon - Gislason - Beer, and Sons",
        version: 3
      }
    ];

    describe('adding a new app', () => {
      beforeEach(() => {
        spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});
      });

      it('creates a new app object', () => {
        const data = {
          apdex: 68,
          contributors: ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."],
          name: "Small Fresh Pants - Kautzer - Boyer, and Sons",
          version: 7
        };
        const expectedOutput = {
          apdex: 68,
          contributors: ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."],
          host: ["7e6272f7-098e.dakota.biz"],
          name: "Small Fresh Pants - Kautzer - Boyer, and Sons",
          version: 7
        };

        let actualOutput = addAppToHosts(['7e6272f7-098e.dakota.biz'], data);
        expect(actualOutput.length).toBe(6);
        expect(actualOutput[actualOutput.length - 1]).toEqual(expectedOutput);
      });
    });

    describe('when app already exists', () => {
      it('adds host name to the host list', () => {
        spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});

        const data = {
          apdex: 68,
          contributors: ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."],
          name: "Small Fresh Pants - Kautzer - Boyer, and Sons",
          version: 7
        };
        const expectedOutput = {
          apdex: 68,
          contributors: ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."],
          host: ["7e6272f7-098e.dakota.biz", "90210.doctor.ca"],
          name: "Small Fresh Pants - Kautzer - Boyer, and Sons",
          version: 7
        };

        let actualOutput = addAppToHosts(['90210.doctor.ca'], data);
        expect(actualOutput.length).toBe(applications.length); // unchanged

        let modifiedApp = actualOutput.find((app) => app.name === data.name);
        expect(modifiedApp).toEqual(expectedOutput);
      });

      describe('when hostName is present already', () => {
        it('halts the operation and does not modify the app', () => {
          spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});

          const data = {
            apdex: 40,
            contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
            name: "Small Rubber Sausages - Kutch LLC, and Sons",
            version: 5
          };
          const expectedOutput = {
            apdex: 40,
            contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
            host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
            name: "Small Rubber Sausages - Kutch LLC, and Sons",
            version: 5
          };

          let actualOutput = addAppToHosts(['7e6272f7-098e.dakota.biz'], data);
          expect(actualOutput.length).toBe(applications.length); // unchanged

          let modifiedApp = actualOutput.find((app) => app.name === data.name);
          expect(modifiedApp).toEqual(expectedOutput);
        });
      });
    });
  });

  describe('#removeAppFromHosts', () => {
    const applications = [
      {
        apdex: 33,
        contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
        host: ["92116865-5462.conor.com", "128406fc-0d3f.tiana.biz", "e7bf58af-f0be.dallas.biz", "7e6272f7-098e.dakota.biz"],
        name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
        version: 7
      },
      {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      },
      {
        apdex: 40,
        contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Small Rubber Sausages - Kutch LLC, and Sons",
        version: 5
      }
    ];

    describe('when apps contains host', () => {
      it('removes the host from the app', () => {
        spyOn(ApiFunctions, 'getData').and.returnValue(applications);
        spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});

        const expectedOutput = [
          {
            apdex: 99,
            contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
            host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
            name: "Sleek Granite Chips - Labadie Inc, LLC",
            version: 4
          },
          {
            apdex: 33,
            contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
            host: ["92116865-5462.conor.com", "128406fc-0d3f.tiana.biz", "e7bf58af-f0be.dallas.biz", "7e6272f7-098e.dakota.biz"],
            name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
            version: 7
          }
        ];

        let actualOutput = removeAppFromHosts(['7e6272f7-098e.dakota.biz'], 'Small Rubber Sausages - Kutch LLC, and Sons', 2);
        expect(actualOutput.length).toEqual(2);
        expect(actualOutput).toEqual(expectedOutput);
      });
    });
  });

  describe('#removeAppFromHost', () => {
    it('removes host from the supplied app', () => {
      const app = {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      };
      const expectedOutput = {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["92116865-5462.conor.com", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      };

      let actualOutput = removeAppFromHost("7e6272f7-098e.dakota.biz", app);

      expect(actualOutput).toBe(true);
      expect(app).toEqual(expectedOutput);
    });

    describe('when hostname not found in app', () => {
      it('does not modify the app', () => {
        const app = {
          apdex: 99,
          contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
          host: ["92116865-5462.conor.com", "e7bf58af-f0be.dallas.biz"],
          name: "Sleek Granite Chips - Labadie Inc, LLC",
          version: 4
        };
        const expectedOutput = {
          apdex: 99,
          contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
          host: ["92116865-5462.conor.com", "e7bf58af-f0be.dallas.biz"],
          name: "Sleek Granite Chips - Labadie Inc, LLC",
          version: 4
        };

        let actualOutput = removeAppFromHost("7e6272f7-098e.dakota.biz", app);

        expect(actualOutput).toBe(false);
        expect(app).toEqual(expectedOutput);
      })
    })
  });
});
