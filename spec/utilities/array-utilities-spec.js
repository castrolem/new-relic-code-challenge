import {flatten} from '../../src/js/utilities/array-utilities';

describe('Array Utilities', () => {
  describe('flatten', () => {
    it('returns a flattened array', () => {
      const input = [1, [2], 3, [4, 5]];
      const actualOutput = flatten(input);
      const expectedOutput = [1, 2, 3, 4, 5];

      expect(actualOutput).toEqual(expectedOutput);
    });

    describe('with deeply nested arrays', () => {
      it('returns a flattened array', () => {
        const input = [[1,2,[3, [4]]], 5];
        const actualOutput = flatten(input);
        const expectedOutput = [1, 2, 3, 4, 5];

        expect(actualOutput).toEqual(expectedOutput);
      });
    })
  })
});
