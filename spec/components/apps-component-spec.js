import * as WindowUtilities from '../../src/js/utilities/window-utilities';
import {shapeAppsObject} from '../../src/js/components/apps-component';

describe('Apps Component', () => {
  describe('#shapeApps', () => {
    const applications = [
      {
        apdex: 33,
        contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
        host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
        name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
        version: 7
      },
      {
        apdex: 99,
        contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
        host: ["7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
        name: "Sleek Granite Chips - Labadie Inc, LLC",
        version: 4
      },
      {
        apdex: 40,
        contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
        host: ["92116865-5462.ranger.com"],
        name: "Small Rubber Sausages - Kutch LLC, and Sons",
        version: 5
      }
    ];

    it('returns an array of hosts with top apps', () => {
      spyOn(WindowUtilities, 'getWindow').and.returnValue({apps: applications});

      const expectedOutput = [
        {
          host: "92116865-5462.conor.com",
          apps: [
            {
              apdex: 33,
              contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
              host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
              name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
              version: 7
            }
          ]
        }, {
          host: "7e6272f7-098e.dakota.biz",
          apps: [
            {
              apdex: 99,
              contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
              host: ["7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
              name: "Sleek Granite Chips - Labadie Inc, LLC",
              version: 4
            },
            {
              apdex: 33,
              contributors: ["Karson III", "Lydia Lind", "Martin Weber"],
              host: ["92116865-5462.conor.com", "7e6272f7-098e.dakota.biz"],
              name: "Awesome Wooden Sausages - Schaefer - Hegmann, Inc",
              version: 7
            }
          ]
        }, {
          host: "e7bf58af-f0be.dallas.biz",
          apps: [
            {
              apdex: 99,
              contributors: ["Madisyn Lesch", "Beryl Howell", "Leone Weimann", "Delaney Mante DVM"],
              host: ["7e6272f7-098e.dakota.biz", "e7bf58af-f0be.dallas.biz"],
              name: "Sleek Granite Chips - Labadie Inc, LLC",
              version: 4
            }
          ]
        }, {
          host: "92116865-5462.ranger.com",
          apps: [
            {
              apdex: 40,
              contributors: ["Ms. Steve Pagac", "Elias Bradtke", "Devan Cummings", "Jennifer Barton", "Tania Cremin"],
              host: ["92116865-5462.ranger.com"],
              name: "Small Rubber Sausages - Kutch LLC, and Sons",
              version: 5
            }
          ]
        }
      ];

      let actualOutput = shapeAppsObject(applications);
      expect(actualOutput).toEqual(expectedOutput);
    });
  });
});
